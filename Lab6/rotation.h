//
// Created by Katherina on 03.11.2020.
//

#ifndef AAA_ROTATION_H
#define AAA_ROTATION_H
#include "bmp_struct.h"
#include <math.h>

struct image* rotate(struct image const* picture);

#endif //AAA_ROTATION_H
